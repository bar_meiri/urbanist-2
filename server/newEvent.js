Meteor.methods({
    addEvent: function (doc) {
        if (!Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }

        var g = NewEventGuests.find().fetch();

        Events.insert({

            name: doc.eventName,
            date: doc.eventDate.toDateString(),
            eventGuests: '',
            owner: Meteor.userId()
        }, function (error, result) {
            g.forEach(function (guest) {
                guest.eventId = result;
            })

            Events.update({_id: result}, {$set: {eventGuests: g}});
        });

        NewEventGuests.remove({});

    },
    clearGuests: function(){
        NewEventGuests.remove({});
    },
    addGuest: function (doc) {
        if (Meteor.user().username === doc.newGuest)
            return;
        if (NewEventGuests.findOne({name: doc.newGuest}))
            return;

        if (Meteor.users.findOne({username: doc.newGuest})) {

            var text = doc.newGuest;
            NewEventGuests.insert({
                name: text,
                status: "Attending",
                imageUrl: "http://lorempixel.com/50/50/people/"
            });
        }
    }

});
