Meteor.methods({
    updateStatus: function(guest){

        if (guest.status == "Attending"){
            var status = "Maybe";
        } else if (guest.status == "Maybe"){
            var status = "Not Attending";
        } else {
            var status = "Attending";
        };

        Events.update({_id : guest.eventId, 'eventGuests._id': guest._id },
            { $set: {'eventGuests.$': {_id: guest._id, name: guest.name ,
                status: status, imageUrl: guest.imageUrl, eventId: guest.eventId}}});
    },
    removeGuest: function(guest){
        Events.update({_id: guest.eventId}, {$pull : {eventGuests : guest}});
    },
    addGuestToEvent: function(event, guestName){

        var addedGuest = Meteor.users.findOne({username: guestName});
        if (addedGuest) //if is user
        {
            var currentEvent = Events.findOne({_id: event._id});

            if (addedGuest._id === currentEvent.owner)
            return;

            var alreadyExists = false;
            Events.findOne({_id: event._id}).eventGuests.forEach(function(x){
                if ( x.name === guestName) {
                    alreadyExists = true;
                }
            });
            if (alreadyExists)
                return;


            var id = event._id;
            var guest = {
                _id: id+guestName,
                name: guestName,
                status: "Attending",
                imageUrl: "http://lorempixel.com/50/50/people/",
                eventId: id
            };
            Events.update({_id: id}, {$push : {eventGuests : guest}});
        }

    }
});
