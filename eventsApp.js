Events = new Mongo.Collection("events");
NewEventGuests = new Mongo.Collection("guests");



if (Meteor.isServer) {

    Meteor.publish("guests", function () {
        return NewEventGuests.find();
    });

    Meteor.publish("events", function () {
        return Events.find();
    });


}