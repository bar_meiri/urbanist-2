
Meteor.subscribe("events");

Template.eventGuest.events({
    'click input' : function () {
        Meteor.call("updateStatus", this);
    },
    "click .delete": function () {
        Meteor.call("removeGuest", this);
        return false;
    }
});

Template.eventGuestList.events({
    "submit .addNewGuest": function (event) {
        var username = event.target.text.value;
        Meteor.call("addGuestToEvent", this, username);
        event.target.text.value = "";
        return false;
    }
});


Template.body.helpers({
    events: function () {
        return Events.find({});
    }
});