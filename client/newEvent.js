Meteor.subscribe("guests");

Template.newGuestForm.helpers({
    guests: function(){
        return NewEventGuests.find({});
    }
});

Template.newGuestForm.helpers({
    users: function() {
        return Meteor.users.find().fetch().map(function(it){
            return it.username; });
    }
});


Template.clear.events({
    'click input' : function () {
        Meteor.call("clearGuests");
    }
});

Template.newEventForm.helpers({
    newEventSchema: function(){
        return new SimpleSchema({
            eventName: {
                type: String,
                label: "Name",
                max: 100,
                optional: false
            },
            eventDate: {
                type: Date,
                label: "Date",
                optional: false,
                min: new Date("2015-01-01T00:00:00.000Z")
            }
        });
    }
});

Template.newGuestForm.helpers({
    newGuestSchema: function(){
        return new SimpleSchema({
            newGuest: {
                type: String,
                max: 100,
                optional: true
            }
        });
    }
});